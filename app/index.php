<?php
    require_once 'conexao.php';

    $query = "SELECT * FROM mensagens";
    $result = $link->query($query)
?>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8"/>
<title>Aplicação Exemplo</title>
<link rel="stylesheet" type="text/css" href="css.css" media="screen" />
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<script src="js.js"> </script>
</head>
<body>


<div class="container">  
  <form id="contact" method="post">
    <h3>App de exemplo</h3>
    <h4>Deixe a sua mensagem!</h4>
    <fieldset>
      <input placeholder="Seu Nome" type="text" tabindex="1" id="nome" required autofocus>
    </fieldset>
    <fieldset>
      <textarea placeholder="Digite a sua mensagem aqui......" id="mensagem" tabindex="5" required></textarea>
    </fieldset>
    <fieldset>
      <button type="button" name="btn_salvar"  id="btn_gravar">Enviar</button>
    </fieldset>
  </form>

  <div class="container">
    <table id="tabela">
      <thead>
        <tr>
          <th>Nome</th>
          <th>Mensagem</th>
        </tr>
      </thead>
      <tbody>
        <?php
          if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
              echo "<tr><td>" . $row["nome"]. "</td><td>" . $row["comentario"]. "</td></tr>";
            }
          } else {
            echo "0 results";
          }
        ?>
      </tbody>
    </table>

  <div id="resposta"></div>
 
  <script src="js.js"> </script>
  
</div>

</body>
</html>